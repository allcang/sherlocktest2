package test;

public class SequenceImpl implements Sequence{
    private int number;
    @Override
    public int getNumber() {
        number++;
        return number;
    }

    public static void main(String[] args) {
        Sequence s = new SequenceImpl();
        Thread t1 = new Mythread(s);
        Thread t2= new Mythread(new SequenceImpl());
        Thread t3 = new Mythread(new SequenceImpl());
        t1.start();
        t2.start();
        t3.start();
    }

}
